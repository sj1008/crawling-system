var i = 1;
var counter = 1;

function addInputs() {
    i++;
    var html = '';
    html += '<div class="input-group mb-3">';
    html += '<div class="input-group-prepend">';
    html += '<div class="input-group-text">';
    html += '<span class="fa fa-bars"></span>';
    html += '</div>';
    html += '</div>';
    html += '<input type="text" class="form-control url-value" placeholder="Enter Url ' + i + '" onkeypress="removeErrorClass(this)" required >';
    html += '</div>';
    $("#sortable").append(html);
}

function fetchUrl() {
    var errorMsg = '';
    var postData = [];
    var sendServerCall = true;
    errorMsg = '<div class="invalid-feedback">Please provide a valid url</div>';
    $(".url-value").each(function () {
        if ($(this).val() == '') {
            $(this).addClass('is-invalid');
            $(this).attr('Placeholder', 'Please enter url');
            sendServerCall = false;
            return false;
        } else if (!isValidUrl($(this).val())) {
            $(this).addClass('is-invalid');
            $(this).nextAll().remove();
            $(this).after(errorMsg);
            $(this).attr('Placeholder', 'Please enter valid url');
            sendServerCall = false;
            return false;
        } else {
            postData.push($(this).val());
        }
    });
    if (sendServerCall) {
        callServer(postData);
    }
}

function isValidUrl(url) {
    var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
        '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return (pattern.test(url));
}

function removeErrorClass(element) {
    $(element).removeClass('is-invalid');
    $(element).find('.invalid-feedback').remove();
}

function callServer(postData) {
    $("#fetch").hide();
    $("#fetching").removeClass('invisible');
    var urlLenght = postData.length;
    var url = (postData).toString();
    es = new EventSource('search.php?q=' + url);
    es.onmessage = function (e) {
        //$(".progress").removeClass('invisible');
        var result = JSON.parse(e.data);
        if (e.lastEventId == 'CLOSE') {
            es.close();
            $(".pbar").html(parseInt(result.progress) + '% complete');
        } else {
            addLog(result.message);
            $(".pbar").html(parseInt(result.progress) + '% complete');
        }
    };
    es.addEventListener('error', function (e) {
        $("#fetching").html('Done');
        es.close();
    });

}

function addLog(message) {
    var r = document.getElementById('results');
    r.innerHTML += '<li class="list-group-item">URL ' + counter + ' ->' + message + '</li>';
    r.scrollTop = r.scrollHeight;
    counter++;
}

// Sortable List
$(function () {
    $("#sortable").sortable();
    $("#sortable").disableSelection();
});
