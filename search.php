<?php
require('_require.php');
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');
$urls = explode(',', $_GET['q']);
function send_message($id, $message, $progress) {
    $d = array('message' => $message , 'progress' => $progress);
    echo "id: $id" . PHP_EOL;
    echo "data: " . json_encode($d) . PHP_EOL;
    echo PHP_EOL;
    ob_flush();
    flush();
}

$percent = (100 / count($urls) );
$urlArr = array();
$pageTitleArr = array();
foreach($urls as $key => $value) {
    $i = $key ? $key+1 : 1;
    if(!in_array($value, $urlArr)){
        array_push($value,$urlArr);
        $scrapperObj = new Scrapper();
        $pageTitle = $scrapperObj->scrapeExternal($value);
        array_push($pageTitle, $pageTitleArr);
    }else{
        $pageTitle = $pageTitleArr[$key];
    }
    send_message($key,$pageTitle, $i*$percent);
    sleep(1);
}