<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSS -->
<?php
if(!empty($stylsheets)) {
    foreach($stylsheets as $styleFile) {
        echo '<link href="' . ASSETS_PATH . 'css/' . $styleFile .'" rel="stylesheet">'."\n";
    }
}
?>
<!-- Web Fonts -->
<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">