<?php
// Css and head meta.
require('_meta-head.php'); ?>
<title><?php echo $metaTitle?></title>
</head>

<body>
<?php require('contents/' . $viewFile);

// JS files and body closing tag.
require('_footer.php');
?>