<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light text-center">
        <a class="navbar-brand">Crawling System</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    </nav>
</header>

<div class="container mt-3">
    <div class="row">
        <div class="col-md-10">
            <div id="sortable">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text fa fa-bars"></span>
                    </div>
                    <input type="text" class="form-control url-value" placeholder="Enter Url 1" onkeypress="removeErrorClass(this)">
                </div>
            </div>

        </div>
        <div class="col-md-2">
            <button class="btn btn-success" onclick="addInputs();">Add</button>
        </div>
    </div>
    <button class="btn btn-success" id="fetch" onclick="fetchUrl();"> Fetch </button>
    <button class="btn btn-success invisible" id="fetching">
        <span class="spinner-border spinner-border-sm"></span>
        Fetching...
    </button>
    <div class="pbar my-3"></div>
    <ul class="list-group" id="results">
    </ul>
</div>