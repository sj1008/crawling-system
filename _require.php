<?php
session_start();
require('_configs.php');
require('_global.php');
function autoloader($classname) {
    $filename = CLASS_PATH . $classname . '.php';
    if (file_exists($filename)) {
        require_once($filename);
    }
}
spl_autoload_register('autoloader');
