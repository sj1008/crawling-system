<?php
/**
 * Global functions.
 *
 * Keep this to an absolute minimum. Any functions used
 * here should be truly global, side-effect free, and not depend on
 * any library code. Its usage must be in hundreds
 * of places, this is not the place for it.
 */

 /**
 * Return the value at $offset in $array if set, or $default if not.
 *
 * @param mixed $array   Array to inspect.
 * @param mixed $offset  Offset into $array.
 * @param mixed $default Value to return if $array[$offset] is not set.
 *
 * @return mixed $array[$offset] or $default
 */
function ifseta($array, $offset, $default = null) {
    return is_array($array) && isset($array[$offset]) ? $array[$offset] : $default;
}

function pre($var, $exit = true) {
    echo "<pre>";
    print_r($var);
    echo "</pre>";
    if ($exit) {
        exit;
    }
}
