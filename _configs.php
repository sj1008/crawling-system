<?php
/**
 * Define constants to be used across the application.
/*---------------------------------------------------------------
 * Set APPLICATION ENVIRONMENT
 *---------------------------------------------------------------
 */
define('ENVIRONMENT', 'development');
if (defined('ENVIRONMENT')) {
    switch (ENVIRONMENT) {
        case 'development':
            error_reporting(E_ALL);
        break;
        case 'testing':
            error_reporting(E_ALL & ~(E_STRICT|E_NOTICE));
        break;
        case 'production':
            error_reporting(0);
        break;
        default:
            exit('Set application environment correctly.');
    }
}
/*---------------------------------------------------------------
 * OTHER CONSTANTS.
 *---------------------------------------------------------------
 */
define('ASSETS_PATH', 'assets/');
define('BASE_PATH', __DIR__);
define('CLASS_PATH', __DIR__ . '/classes/');
define('VIEW_PATH', __DIR__ . '/views/');
