<?php
Class PageHelpers{
    /**
     * Common stylesheets to be added into <head>.
     * @return array List  of stylesheets.
     */
    public function getSharedStylesheets() {
        $sheets = array(
            'bootstrap.min.css',
            'jquery-ui.css',
            'style.css'
		);
        return $sheets;
    }

    /**
     * Common Js files be added into before </body> tag.
     * @return array List  of Scripts.
     */
    public function getSharedJsFiles() {
        $files = array (
            'jquery-1.12.4.js',
            'jquery-ui.js',
            'bootstrap.min.js',
            'search-query.js'
        );
        return $files;
    }
}
