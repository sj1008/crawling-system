<?php
class Scrapper {    
    protected static $errorMsgs = array(
        'SCRAPE_DETECTED' => 'Google blocked us',
        );
    
    /**
     * Scrap external url.
     * 
     * @param string  $url       Url to scrap from.*
     * @return scrape content
     */
    public function scrapeExternal($url) {
        $curlSession = $this->newCurlSession($url);
        $ch = $curlSession['curlRsource'];
        $htmdata = curl_exec($ch);
            if (!$htmdata) {
            $error = curl_error($ch);
            $info = curl_getinfo($ch);
            $errorMsg = "\tError scraping: $error [ $error ]";
            $this->processError($errorMsg);
            return 'No Title';
        }
        curl_close($ch);
        $this->isContentGood($htmdata);
        return $this->getPageTitle($htmdata);
    }    
    
    public function newCurlSession($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.0; en; rv:1.9.0.4) Gecko/2009011913 Firefox/3.0.6');
        return array('curlRsource' => $ch);
    }
    
    protected function processError($errorMsg) {
        // For any other environment just display the error for debug purpose.
        $printError = ifseta(self::$errorMsgs, $errorMsg, $errorMsg);
        pre($printError, false);
    }

    protected function isContentGood($htmdata){
        $errorMsg = false;
        if (strstr($htmdata, 'http://error.google.com')) {
            $errorMsg = 'SCRAPE_DETECTED';
        } else if (strstr($htmdata, 'Our systems have detected unusual traffic from your computer')) {
            $errorMsg = 'SCRAPE_DETECTED';
        }
        if ($errorMsg) {
            $this->processError($errorMsg);
        }
        return true;
    }

    protected function getPageTitle($content){
        $dom = new DOMDocument;
        @$dom->loadHTML($content);
        $list= $dom->getElementsByTagName('title');
        $title = 'No Title';
        if ($list->length > 0) {
            $title = $list->item(0)->textContent;
        }
        return $title;

    }
}